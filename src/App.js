import React from "react";
import "./App.scss";
import ImageUploader from "./imageUploader";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { image: "" };
    this.onDrop = this.onDrop.bind(this);
  }

  onDrop(image) {
    this.setState({
      image: image,
    });
  }

  render() {
    return (
      <div className="uploader-container">
        <ImageUploader
          onChange={this.onDrop}
          onUploadClick={this.uploadImage}
        />
      </div>
    );
  }
}

export default App;
