# ML Screenshots Documentation

## About

The goal of this project was to demonstrate building a simple model to compare iOS and Android screenshots. We could then use this model to determine, with confidence, than an image was an Android or iOS screenshot.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.


## Project Pieces

### Front End Repository (this one)

The front-end part of the project is used to receive an image from the user and send it across to the API where the model is run to determine whether it is most likely an iOS or Android screenshot. The front-end uses file-picking to convert the image to a base64 string. This string is then sent to the API along with the region and endpoint needed to process the image.

## API Gateway

This is an AWS API Gateway that the front-end talks to. The API gateway helps route the request to the Lambda endpoint in AWS. When we were originally working on this project, we had quite a few issues with CORS blocking requests to the Lambda endpoint.

## Lambda 

The Lambda is a small JavaScript function that receives a base64 image string, a region, and a hosted model to compare the image against. It then uses the model hosted on Sagemaker to determine the confidences that the image is an iOS screenshot or an Android screenshot. The endpoint then sends a response with these confidences to be displayed by the front-end.

## Sagemaker Model

The Sagemaker Model is hosted on an endpoint in Sagemaker. This endpoint is called through a lambda and runs the model to determine the confidences of an iOS and Android image. The confidences are returned to the Lambda.

## Sagemaker Training

Currently, our project uses training setup in Sagemaker to generate a new model and hosted endpoint for it. We did manage to get this to work, but it is not 100% ideal. Because the information is contained within the training options, we don't really have a way to document what we did or why we did it. We also can't easily make changes or revert if we mess up something or end up with a less accurate model. Because of this, we feel that eventually, the model should be converted to a Jupyter notebook hosted in Sagemaker. We could then use the notebook to spin up the model/endpoint after training.
