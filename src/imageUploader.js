import React from "react";
import PropTypes from "prop-types";
import "./imageUploader.scss";
// import UploadIcon from './UploadIcon.svg';
import axios from "axios";
import { DisappearedLoading } from "react-loadingg";

const API_ENDPOINT =
  "https://plb4rc7cx7.execute-api.us-east-1.amazonaws.com/dev/send-image";
// const MAX_IMAGE_SIZE = 1000000;

const ERROR = {
  NOT_SUPPORTED_EXTENSION: "NOT_SUPPORTED_EXTENSION",
  FILESIZE_TOO_LARGE: "FILESIZE_TOO_LARGE",
};

class ReactImageUploadComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      image: null,
      files: [],
      fileErrors: [],
      androidPrediction: null,
      iosPrediction: null,
      showLoading: false,
    };
    this.inputElement = "";
    this.onDropFile = this.onDropFile.bind(this);
    this.uploadImage = this.uploadImage.bind(this);
    this.updatePredictions = this.updatePredictions.bind(this);
    // this.triggerFileUpload = this.triggerFileUpload.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.files !== this.state.files) {
      this.props.onChange(this.state.files, this.state.image);
    }
  }

  /*
   Load image at the beggining if defaultImage prop exists
   */
  componentWillReceiveProps(nextProps) {
    if (nextProps.defaultImages !== this.props.defaultImages) {
      this.setState({ image: nextProps.defaultImages });
    }
  }

  /*
	 Check file extension (onDropFile)
	 */
  hasExtension(fileName) {
    const pattern =
      "(" + this.props.imgExtension.join("|").replace(/\./g, "\\.") + ")$";
    return new RegExp(pattern, "i").test(fileName);
  }

  /*
   Handle file validation
   */
  onDropFile(e) {
    if (!e.target.files.length) {
      return;
    } else {
      this.setState({
        androidPrediction: null,
        iosPrediction: null,
      });

      const files = e.target.files;
      const allFilePromises = [];
      const fileErrors = [];

      // Iterate over all uploaded files
      for (let i = 0; i < files.length; i++) {
        let file = files[i];
        let fileError = {
          name: file.name,
        };
        // Check for file extension
        if (!this.hasExtension(file.name)) {
          fileError = Object.assign(fileError, {
            type: ERROR.NOT_SUPPORTED_EXTENSION,
          });
          fileErrors.push(fileError);
          continue;
        }
        // Check for file size
        if (file.size > this.props.maxFileSize) {
          fileError = Object.assign(fileError, {
            type: ERROR.FILESIZE_TOO_LARGE,
          });
          fileErrors.push(fileError);
          continue;
        }

        allFilePromises.push(this.readFile(file));
      }

      this.setState({
        fileErrors,
      });

      Promise.all(allFilePromises).then((newFilesData) => {
        const dataURLs = [];
        const files = [];

        newFilesData.forEach((newFileData) => {
          dataURLs.push(newFileData.dataURL);
          files.push(newFileData.file);
        });

        this.setState({ image: dataURLs, files: files });
      });
    }
  }

  trimBase64(base) {
    return base[0].split(",")[1].toString();
  }

  updatePredictions(response) {
    this.setState({
      androidPrediction: response.data.prediction[0],
      iosPrediction: response.data.prediction[1],
    });
    console.log(
      "ANDROID PREDICTION:",
      this.state.androidPrediction,
      "IOS PREDICTION:",
      this.state.iosPrediction
    );
  }

  async uploadImage() {
    console.log("Upload clicked a;lfdskl;afs");
    this.setState({
      showLoading: true,
    });
    let imageData = this.state.image;
    imageData = this.trimBase64(imageData);
    console.log("DATA", imageData);
    const body = {
      endpointRegion: "us-east-1",
      endpointName: "platform-screenshot-endpoint",
      base64Image: imageData,
    };
    console.warn(body);
    axios
      .post(API_ENDPOINT, body)
      .then((response) => {
        console.log("RESPONSE", response);
        this.updatePredictions(response);
      })
      .then(() => {
        this.setState({
          showLoading: false,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  /*
     Read a file and return a promise that when resolved gives the file itself and the data URL
   */
  readFile(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();

      // Read the image via FileReader API and save image result in state.
      reader.onload = function (e) {
        // Add the file name to the data URL
        let dataURL = e.target.result;
        dataURL = dataURL.replace(";base64", `;name=${file.name};base64`);
        resolve({ file, dataURL });
        console.warn(dataURL);
      };
      console.warn("FILE:", file);
      reader.readAsDataURL(file);
    });
  }

  /*
   Remove the image from state
   */
  removeImage() {
    this.setState(
      {
        image: null,
        files: [],
        androidPrediction: null,
        iosPrediction: null,
      },
      () => {
        document.getElementById("input").value = "";
      }
    );
  }

  /*
   Check if any errors && render
   */
  renderErrors() {
    const { fileErrors } = this.state;
    return fileErrors.map((fileError, index) => {
      return (
        <div className={"errorMessage " + this.props.errorClass} key={index}>
          * {fileError.name}{" "}
          {fileError.type === ERROR.FILESIZE_TOO_LARGE
            ? this.props.fileSizeError
            : this.props.fileTypeError}
        </div>
      );
    });
  }

  /*
   Render the upload icon
   */
  //   renderIcon() {
  //     if (this.props.withIcon) {
  //       return <img src={UploadIcon} className="uploadIcon"	alt="Upload Icon" />;
  //     }
  //   }

  /*
   Render label
   */
  renderLabel() {
    if (this.props.withLabel) {
      return (
        <div>
          <h1 className={this.props.labelClass}>iOS or Android?</h1>
          <p>Screenshot Prediction Machine</p>
        </div>
      );
    }
  }

  /*
   Render preview images
   */
  renderPreview() {
    return (
      <div className="uploadImageWrapper">{this.renderPreviewimage()}</div>
    );
  }

  renderPreviewimage() {
    if (this.state.image) {
      return (
        <div className="uploadPictureContainer">
          <div className="deleteImgWrapper">
            <div
              className="deleteImage"
              onClick={() => this.removeImage()}
            ></div>
          </div>
          <img src={this.state.image} className="uploadPicture" alt="preview" />
        </div>
      );
    }
  }

  /*
   On button click, trigger input file to open
   */
  // triggerFileUpload() {
  //   this.inputElement.click();
  // }

  // clearimage() {
  //   this.setState({image: []})
  // }

  render() {
    return (
      <div className={"fileUploader " + this.props.className}>
        <div className="fileContainer">
          {this.renderLabel()}
          <div className="errorsContainer">{this.renderErrors()}</div>
          <div className="chooseFileWrapper">
            <button className="chooseFileBtn">Choose File</button>
            <input
              type="file"
              ref={(input) => (this.inputElement = input)}
              name={this.props.name}
              multiple={false}
              onChange={this.onDropFile}
              accept={this.props.accept}
              id="input"
            />
          </div>
          {this.renderPreview()}
        </div>
        {this.state.image ? (
          <button onClick={this.uploadImage} className="uploadBtn">
            Upload File
          </button>
        ) : null}
        {this.state.showLoading ? (
          <div className="loadingSymbol">
            <DisappearedLoading color="white" />
          </div>
        ) : null}
        {this.state.androidPrediction && this.state.image ? (
          <div className="predictionsWrapper">
            <div className="prediction">
              Android Prediction:{" "}
              {Math.round(this.state.androidPrediction * 100)}%
            </div>
            <div className="prediction">
              iOS Prediction: {Math.round(this.state.iosPrediction * 100)}%
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

ReactImageUploadComponent.defaultProps = {
  className: "",
  buttonClassName: "",
  accept: "image/*",
  name: "",
  //   withIcon: true,
  buttonType: "button",
  withLabel: true,
  label: "Max file size: 5mb, accepted: jpg|gif|png",
  imgExtension: [".jpg", ".jpeg", ".gif", ".png"],
  maxFileSize: 5242880,
  fileSizeError: " file size is too big",
  fileTypeError: " is not a supported file extension",
  errorClass: "",
  singleImage: false,
  onChange: () => {},
  defaultImages: [],
};

ReactImageUploadComponent.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func,
  onDelete: PropTypes.func,
  buttonClassName: PropTypes.string,
  buttonType: PropTypes.string,
  accept: PropTypes.string,
  name: PropTypes.string,
  //   withIcon: PropTypes.bool,
  buttonText: PropTypes.string,
  withLabel: PropTypes.bool,
  label: PropTypes.string,
  labelClass: PropTypes.string,
  imgExtension: PropTypes.array,
  maxFileSize: PropTypes.number,
  fileSizeError: PropTypes.string,
  fileTypeError: PropTypes.string,
  errorClass: PropTypes.string,
  singleImage: PropTypes.bool,
  defaultImages: PropTypes.array,
};

export default ReactImageUploadComponent;
